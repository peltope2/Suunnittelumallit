/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package State;

/**
 *
 * @author petra
 */
public class PelihahmoContext implements State {
     
    private State currentState; 

    public PelihahmoContext() {
       setState(new Charmander()); 
    }
    
    public void setState(final State newState) {
        currentState= newState; 
    }

    @Override
    public void hyokkaa() {
        currentState.hyokkaa(); 
    }

    @Override
    public void puolusta() {
        currentState.puolusta();
    }

    @Override
    public void pakene() {
        currentState.pakene(); 
    }

    @Override
    public void tulostaTila() {
        currentState.tulostaTila(); 
    }

   
    
}

