/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package State;

/**
 *
 * @author petra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        final PelihahmoContext player = new PelihahmoContext();
        
        player.tulostaTila();
        player.hyokkaa();
        player.puolusta();
        player.pakene();
        System.out.println("");
        
        player.setState(new Charmeleon());
        player.tulostaTila();
        player.hyokkaa();
        player.puolusta();
        player.pakene();
        System.out.println("");
        
        player.setState(new Charizard());
        player.tulostaTila();
        player.hyokkaa();
        player.puolusta();
        player.pakene();
    }
    
    
}
