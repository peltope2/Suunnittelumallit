/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Composite;

/**
 *
 * @author petra
 */

import java.util.ArrayList;
import java.util.List;

public class Emolevy implements LaiteosaComposite {

    private double hinta;
    private List<Laiteosa> emolevynOsat = new ArrayList<>();
    
    public Emolevy(double hinta) {
        this.hinta = hinta;
    }
    
    @Override
    public double getHinta() {
        double yhtHinta = this.hinta;
        for (Laiteosa laiteosa : emolevynOsat) {
            yhtHinta += laiteosa.getHinta();
        }
        return yhtHinta;
    }
    
    @Override
    public void addLaiteosa(Laiteosa osa) {
        if (!emolevynOsat.contains(osa)) {
            emolevynOsat.add(osa);
        }
    }
}
