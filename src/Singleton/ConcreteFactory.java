/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author petra
 */
public enum ConcreteFactory implements AbstractFactory {

    INSTANCE;
    
    private String jotain = "Jotain";

    @Override
    public void doSomething() {
        System.out.println(jotain);
    }

    @Override
    public void doSomethingElse() {
        System.out.println("Jotain muuta");
    }

    @Override
    public void setSomething(String newSomething) {
        jotain = newSomething;
    }
    
}
