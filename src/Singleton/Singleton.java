/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author petra
 */
public class Singleton {

    public static void main(String[] args) {
        AbstractFactory factory = ConcreteFactory.INSTANCE;
        factory.doSomething();
        factory.doSomethingElse();
        factory.setSomething("Jotain on muutettu!");
        
        AbstractFactory secondFactory = ConcreteFactory.INSTANCE;
        secondFactory.doSomethingElse();
        secondFactory.doSomething();
    }
    
}
