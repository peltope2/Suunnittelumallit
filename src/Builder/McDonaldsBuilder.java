/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

/**
 *
 * @author petra
 */
public class McDonaldsBuilder implements HamburgerBuilder {
    
    private StringBuilder sBuilder = new StringBuilder(); 

    @Override
    public void addTopBun() {
        sBuilder.append("Topbun\n"); 
        
    }

    @Override
    public void addLettuce() {
        sBuilder.append("Lettuce\n");
    }

    @Override
    public void addTomato() {
        sBuilder.append("Tomato\n"); 
    }

    @Override
    public void addBeefPatty() {
        sBuilder.append("Beefpatty\n");
    }

    @Override
    public void addSauce() {
        sBuilder.append("Specialsauce\n");
    }

    
    @Override
    public void addMayonnaise() {
    
    }
    
    @Override
    public void addKetchup() {
        sBuilder.append("Ketchup\n");
    }

    @Override
    public void addPickels() {
        sBuilder.append("Pickels\n");
    }

    @Override
    public void addCheese() {
        sBuilder.append("Cheese\n");
    }

    @Override
    public void addBottomBun() {
        sBuilder.append("Bottombun\n");
    }

    @Override
    public Object getBurger() {
        return sBuilder;
    }

           
    
    
}
