/*Kirjoita ohjelma, jossa rakennat kerroshampurilaisen Buildermallin mukaisesti. 
Toteuta vähintään kaksi konkreettista builderia(Hesburger ja McDonalds). 
Toteuta builderit siten, että kummallakin on eri
tietorakenne hampurilaisen koostamiseksi. Toisella esim. List, johon
tallennetaan hampurilaisen osat olioina ja toisella StringBuilder, jossa osia
edustavat merkkijonot. Builderilla on getBurger()-metodi, joka palauttaa
hampurilaisen sellaisena tietorakenteena, jona se on luotu. */

package Builder;

/**
 *
 * @author petra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HamburgerBuilder mcBuilder = new McDonaldsBuilder(); 
        HamburgerBuilder hBuilder = new HesburgerBuilder();
        Director hamburgerDirector = new Director(hBuilder); 
        
        System.out.println("Creating Hesburger hamburger.");
        hamburgerDirector.buildBurger();
        System.out.println("Hesburger hamburger includes: " +hBuilder.getBurger()); 
        System.out.println("");
        
        System.out.println("Creating McDonalds hamburger.");
        hamburgerDirector = new Director(mcBuilder); 
        hamburgerDirector.buildBurger();
        System.out.println("McDonalds hamburger includes: \n" +mcBuilder.getBurger()); 
        System.out.println("");
        
        
        
        
    }
    
}
