/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

/**
 *
 * @author petra
 */
public interface HamburgerBuilder {
        
    void addTopBun(); 
    void addLettuce(); 
    void addTomato(); 
    void addBeefPatty(); 
    void addSauce(); 
    void addMayonnaise(); 
    void addKetchup(); 
    void addPickels(); 
    void addCheese(); 
    void addBottomBun(); 
    Object getBurger(); 
}
