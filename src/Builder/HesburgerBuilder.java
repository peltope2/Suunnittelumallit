/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

import java.util.ArrayList; 
import java.util.List; 

/**
 *
 * @author petra
 */
public class HesburgerBuilder implements HamburgerBuilder {
    
    private List<String> parts = new ArrayList<>(); 

    @Override
    public void addTopBun() {
       parts.add("Topbun");
    }

    @Override
    public void addLettuce() {
       parts.add("Topbun");
    }

    @Override
    public void addTomato() {
        parts.add("Tomato");
    }

    @Override
    public void addBeefPatty() {
        parts.add("Beefpatty");
    }

    @Override
    public void addSauce() {
    
    }

    @Override
    public void addMayonnaise() {
        parts.add("Paprikamayonnaise");
    }

    @Override
    public void addKetchup() {
   
    }

    @Override
    public void addPickels() {
        parts.add("Pickels");
    }

    @Override
    public void addCheese() {
        parts.add("Cheese");
    }

    @Override
    public void addBottomBun() {
        parts.add("Bottombun");
    }

    @Override
    public Object getBurger() {
        return parts; 
    }

    
    
    
    
    
    
}
