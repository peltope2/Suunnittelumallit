/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Builder;

/**
 *
 * @author petra
 */
public class Director {
    
    private HamburgerBuilder builder; 

    public Director(HamburgerBuilder buildet) {
        this.builder = buildet;
    }

    public void buildBurger() {
        builder.addTopBun(); 
        builder.addLettuce();
        builder.addTomato();
        builder.addBeefPatty();
        builder.addSauce();
        builder.addKetchup();
        builder.addPickels();
        builder.addCheese();
        builder.addBottomBun();
    }
    
    
    
}
