/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Iterator;


import java.util.Iterator;
import java.util.List;

/**
 *
 * @author petra
 */
public class IteratorThread extends Thread
{
    private Iterator iterator;
    private List list;
    private boolean sleep;
    public IteratorThread(List list, Iterator iterator){
        this.list = list;
        if(iterator==null){
            this.iterator = list.iterator();
        }else{
            this.iterator=iterator;
        }
    }
    public void setSleep(boolean sleep) {
        this.sleep = sleep;
    }
    @Override
    public void run() {
        while(iterator.hasNext()) {
            System.out.println(iterator.next() + " "+ this.getId());
            try {
                if(sleep) {
                    sleep(500);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}