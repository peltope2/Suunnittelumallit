/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Iterator;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author petra
 */
public class Main {
public static void main(String args[]) throws InterruptedException {
        //List list = Arrays.asList("abcdefghijklmnopqrstuvwxyz".split(""));
        ArrayList list = new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        System.out.println("Molemmilla on omat iteraattorinsa");
        IteratorThread t1 = new IteratorThread(list,null);
        IteratorThread t2 = new IteratorThread(list,null);
        t1.start();
        t2.start();
        Thread.sleep(2000);
        System.out.println("Molemmilla on sama iteraattori");
        Iterator ite = list.iterator();
        t1 = new IteratorThread(list,ite);
        t2 = new IteratorThread(list,ite);
        t1.start();
        t2.start();
        Thread.sleep(2000);
        System.out.println("Muutetaan listaa läpikäynnin aikana");
        t1 = new IteratorThread(list,null);
        t2 = new IteratorThread(list,null);
        t1.setSleep(true);
        t2.setSleep(true);
        t1.start();
        t2.start();
        list.add("a");
        list.add("b");
        list.add("c");
    }
}
