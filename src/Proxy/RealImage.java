/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proxy;

/**
 *
 * @author petra
 */
public class RealImage implements Image {
    
    private String filename = null;
    /**
     * Constructor
     * @param filename
     */
    public RealImage(final String filename) { 
        this.filename = filename;
        loadImageFromDisk();
    }

    /**
     * Ladataan kuva levylle
     */
    private void loadImageFromDisk() {
        System.out.println("Ladataan " + filename);
    }

    /**
     * Näytetään kuva
     */
    public void displayImage() { 
        System.out.println("Näytetään " + filename); 
    }

    @Override
    public String showData() {
         return this.filename; 
    }
    
    
    
}
