
package Proxy;

import java.util.ArrayList; 
import java.util.List;

/**
 *
 * @author petra
 */
public class Proxy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        List<ProxyImage> lista = new ArrayList<>();
        
        for (int i = 0; i<5; i++) { // luodaan kuvat listalle
            lista.add(new ProxyImage("kuva"+(i+1)));
        }
        
        for (ProxyImage kuva : lista) {
            System.out.println("Tarkastellaan "+kuva.showData()+ ":n tietoja \n");
        }
        
        for (ProxyImage kuva : lista) {
            kuva.displayImage();
        }
    }    

}
    
    

