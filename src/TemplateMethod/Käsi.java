/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TemplateMethod;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author petra
 */
public class Käsi {
    private List<Pakka> pakat;
    private List<Kortti> käsi;
    private String nimi;
    private int yhteensä;

    public Käsi(String name, List<Pakka> decks) {
        this.pakat = decks;
        this.käsi = new ArrayList<>();
        this.nimi = name;
    }

    public void nostaKortti() {
        Pakka deck = valitsePakka();
        käsi.add(deck.jaaKortti());
        laskeYhteensä();
    }

    private Pakka valitsePakka() {
        int min = 0;
        int max = pakat.size() - 1;
        return pakat.get(min + (int) (Math.random() * ((max - min) + 1)));
    }

    private void laskeYhteensä() {
        yhteensä = 0;
        boolean hasAnAce = false;
        for (Kortti c : käsi) {            
            int value = c.getValue();
            // In blackjack face cards are all 10
            if (value > 10) {
                yhteensä += 10;
            } else if (value == 1) {
                hasAnAce = true;
                yhteensä += value;
            } else {
                yhteensä += value;
            }
        }

        // Ässä voi olla 1 tai 11, riippuen käden yhteismäärästä (mutta vain 1 yksi ässä kerrallaan voi olla 11)
        if (hasAnAce) {
            if (käsi.size() > 1) {                
                if (yhteensä + 10 <= 21) {
                    yhteensä += 10;
                }
            } else {
                yhteensä += 10;
            }
        }
    }

    public int getYhteensä() {
        return yhteensä;
    }
    
    public int getKäsimäärä() {
        return käsi.size();
    }

    @Override
    public String toString() {
        // 
        String output = nimi + "n käsi: ";
        for (Kortti c : käsi) {
            output += c.getValueAsString() + ", ";
        }
        output = output.substring(0, output.length() - 2);
        output += "\n" + nimi + "n käsi yhteensä: ";
        if (yhteensä == 21 && käsi.size() == 2) {
            output += "BLACKJACK!";
        } else if (yhteensä > 21) {
            output += yhteensä + " Yli meni!";
        } else {
            output += yhteensä;
        }
        return output;
    }
}
