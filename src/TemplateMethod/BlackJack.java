/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TemplateMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 * 
 * @author petra
 */
public class BlackJack extends Peli {

    private final int NUM_OF_DECKS = 4;
    private Scanner scanner = new Scanner(System.in);
    private List<Pakka> pakat;
    private Käsi jakaja;
    private Käsi pelaaja;
    private boolean pelinLoppu = false;
    private boolean pelaajaJää = false;
    private int tulos = 0;
    private final int VOITTO = 1, HÄVIÖ = -1;

    @Override
    void initializeGame() {
        System.out.println("***************************************************");
        System.out.println("| \t           BLACKJACK           \t\t  |");
        System.out.println("| \t                               \t\t  |");
        System.out.println("| \t           SÄÄNNÖT:           \t\t  |");
        System.out.println("| \t       Kuvakortit: 10         \t\t  |");
        System.out.println("| \t       Ässä: 1 tai 11         \t\t  |");
        System.out.println("| \t 17 pistettä: Jakaja jää tähän       \t  |");
        System.out.println("***************************************************");

        // luo pakat
        pakat = new ArrayList<>();
        for (int i = 1; i < NUM_OF_DECKS; i++) {
            pakat.add(new Pakka());
        }

        // Create players
        jakaja = new Käsi("Jakaja", pakat);
        pelaaja = new Käsi("Pelaaja", pakat);

        // Draw initial cards
        jakaja.nostaKortti();
        pelaaja.nostaKortti();
        pelaaja.nostaKortti();

        // Output initial hand
        printStatus();

        // If Player gets 21 in first hand draw second card for jakaja
        if (pelaaja.getYhteensä() == 21) {
            jakaja.nostaKortti();
            printStatus();
        }
        pelinLoppu = checkStatus();
    }

    @Override
    public void makePlay() {
        final int HIT = 1, STAND = 2;
        int choice = Integer.parseInt(scanner.nextLine());

        // Player has 21. Let's not let him/her screw it up
        if (pelaaja.getYhteensä() == 21) {
            choice = STAND;
        }
        
        switch (choice) {
            case HIT:
                pelaaja.nostaKortti();
                printStatus();
                pelinLoppu = checkStatus();
                break;
            case STAND:
                pelaajaJää = true;
                while (!pelinLoppu) {
                    jakaja.nostaKortti();
                    printStatus();
                    pelinLoppu = checkStatus();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                    }
                }
                break;
        }
    }

    private boolean checkStatus() {
        if (pelaajaJää) {
            if (pelaaja.getYhteensä() < jakaja.getYhteensä() && jakaja.getYhteensä() <= 21) {
                tulos = HÄVIÖ;
                return true;
            } else if (pelaaja.getYhteensä() > jakaja.getYhteensä() && jakaja.getYhteensä() > 16) {
                tulos = VOITTO;
                return true;
            } else if (pelaaja.getYhteensä() == jakaja.getYhteensä() && jakaja.getYhteensä() > 16) {
                // tasapeli
                return true;
            }
        }

        if (pelaaja.getYhteensä() == 21 || jakaja.getYhteensä() == 21) {
            // Soft 17
            if (pelaaja.getYhteensä() == 21 && jakaja.getYhteensä() >= 17) {
                // 
                if (pelaaja.getYhteensä() == 21 && jakaja.getYhteensä() != 21) {
                    tulos = VOITTO;
                }                
                return true;
            } else if (pelaaja.getYhteensä() == 21 && pelaaja.getKäsimäärä() == 2) {
                // pelaajalla on blackjack
                jakaja.nostaKortti();
                printStatus();
                if (pelaaja.getYhteensä() == 21 && jakaja.getYhteensä() != 21) {
                    tulos = VOITTO;
                }
                return true;
            } else if (pelaaja.getYhteensä() == 21 && jakaja.getYhteensä() == 21) {
                // tuloksena on tasapeli
                return true;
            }
        }

        if (pelaaja.getYhteensä() > 21 || jakaja.getYhteensä() > 21) {
            if (pelaaja.getYhteensä() < 21) {
                tulos = VOITTO;
                return true;
            } else if (jakaja.getYhteensä() < 21) {
                tulos = HÄVIÖ;
                return true;
            }
        }
        // tulosta ei ole vielä saavutettu
        if (!pelaajaJää) {
            printOptions();
        }
        return false;
    }

    @Override
    public boolean endOfGame() {
        return pelinLoppu;
    }

    @Override
    public void printWinner() {
        if (tulos == VOITTO) {
            System.out.println("Sinä voitit!");
        } else if (tulos == HÄVIÖ) {
            System.out.println("Jakaja voittaa!");
        } else {
            System.out.println("Tasapeli!"); 
        }
    }

    private void printStatus() {
        if (pelaajaJää) {
            System.out.println("Pelaaja jää tähän.");
        }
        System.out.println(jakaja);
        System.out.println(pelaaja);
        System.out.println("");
    }

    private void printOptions() {
        if (pelaaja.getYhteensä() != 21) {
            System.out.println("[1] Lisää kortti [2] Jää tähän");
        } else {
            System.out.println("[2] Jää tähän");
        }
    }
}   
    
