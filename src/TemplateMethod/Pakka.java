/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TemplateMethod;

/**
 *
 * @author petra
 */
public class Pakka {
    /**
     * Pakka sisältää 52 korttia, tai 54 mikäli mukana on 2 Jokerikorttia. 
     */
    private Kortti[] kortti;

    /**
     * Pitää lukua siitä, kuinka monta korttia on jo jaettu. 
     */
    private int korttejakäytetty;

    /**
     * Kortit luodaan järjestyksessä The sekoita() metodi sekoittaa kortit
     * sattumanvaraiseen järjestykseen.
     */
    public Pakka() {
        kortti = new Kortti[52];
        int kortit = 0; // Kuinka monta korttia on luotu tähän saakka
        for (int maa = 0; maa <= 3; maa++) {
            for (int arvo = 1; arvo <= 13; arvo++) {
                kortti[kortit] = new Kortti(arvo, maa);
                kortit++;
            }
        }
        korttejakäytetty = 0;
        sekoita();
    }

    /**
     * Put all the used cards back into the kortti (if any), and sekoita the kortti
 into a random order.
     */
    public void sekoita() {
        for (int i = kortti.length - 1; i > 0; i--) {
            int rand = (int) (Math.random() * (i + 1));
            Kortti temp = kortti[i];
            kortti[i] = kortti[rand];
            kortti[rand] = temp;
        }
        korttejakäytetty = 0;
    }

    /**
     * Jaettujen korttien määrä vähentää pakassa olevien korttien määrää.
     * Tämä funktio palauttaa jäljellä olevien korttien määrän. Se vähenee aina 
     * 1:llä, kun jaaKortti metodia käytetään kortin jakamiseen pelipöydälle.
     */
    public int korttejaJäljellä() {
        return kortti.length - korttejakäytetty;
    }

    /**
     * Jakaa pakan seuravan kortin pelipöydälle. Voit tarkistaa jäljellä olevien 
     * korttien määrän kutsumalla korttejaJäljellä()funktiota.
     * @return Jaettava kortti
     * @throws palauttaa IllegalStateException jos kortteja ei ole jäljellä.
     */
    public Kortti jaaKortti() {
        if (korttejakäytetty == kortti.length) {
            throw new IllegalStateException("No cards are left in the deck.");
        }
        korttejakäytetty++;
        return kortti[korttejakäytetty - 1];
      // Kortteja ei poisteta oikeasti taulukosta.Tarkoitus on vain seurata, montako korttia on jäljellä. 
       
    }

} // end class Pakka
    
    

