/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TemplateMethod;

/**
 *
 * @author petra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Pelataan yksi peli BlackJack-peliä. Uuden pelin pelatakseen on peli aloitettava alusta.    
        Peli bj = new BlackJack();
        bj.playOneGame();
    }

    
}
