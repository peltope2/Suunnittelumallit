
package TemplateMethod;

/**
 *
 * @author petra
 */
public class Kortti {
        
 public final static int PATA = 0;   // Maat korteille.
    public final static int HERTTA = 1;
    public final static int RUUTU = 2;
    public final static int RISTI = 3;

    public final static int ÄSSÄ = 1;      // Arvot kuvallisille korteille. 
    public final static int JÄTKÄ = 10;    //  Korteille, joiden arvo on 2-10 
    public final static int AKKA = 10;   //   lasketaan arvoksi niiden numeerinen arvo.
    public final static int KUNKKU = 10;

    /**
     * Kortin maat, jotka koostuvat arvoistaPATA, HERTTA, RUUTU, RISTI,
     * tai Jokerista. Maata ei voi muuttaa kortin luomisen jälkeen.
     */
    private final int maa;

    /**
     * Kortin arvo. Normaalille kortille arvo on välillä 1-13, jossa ÄSSÄ edustaa arvoa 1. 
    JOKERIn, arvo voi olla mitä vain. Arvoa ei voi muuttaa kortin luomisen jälkeen. 
     */
    private final int arvo;

    /**
     * Luo kortin arvon ja maan 
     *
     * @param KortinArvo on uuden kortin arvo. For a regular card
 (non-joker), the arvo must be in the range 1 through 13, with 1
 representing an Ace. You can use the constants Card.ÄSSÄ, Card.JÄTKÄ,
 Card.AKKA, and Card.KUNKKU. For a Joker, the arvo can be anything.
     * @param theSuit on uuden kortin maa.
     * @throws antaa virheilmoituksen laittomasta parametrin arvosta.
     */
    public Kortti(int KortinArvo, int theSuit) {
        if (theSuit != PATA && theSuit != HERTTA && theSuit != RUUTU
                && theSuit != RISTI) {
            throw new IllegalArgumentException("Laiton kortin maa");
        }
        if ((KortinArvo < 1 || KortinArvo > 13)) {
            throw new IllegalArgumentException("Laiton kortin arvo");
        }
        arvo = KortinArvo;
        maa = theSuit;
    }

    /**
     * Returns the maa of this card.
     *
     * @returns the suit, which is one of the constants Card.SPADES,
     * Card.HEARTS, Card.DIAMONDS, Card.CLUBS, or Card.JOKER
     */
    public int getSuit() {
        return maa;
    }

    /**
     * Returns Palauttaa kortin arvon.
     *
     * @return palauttaa arvon jonka luku voi olla 1-13. Jokerin arvo voi olla mikä tahansa.
     */
    public int getValue() {
        return arvo;
    }

    /**
     *Palauttaa maan Stringinä.
     * @return palauttaa maan 
     */
    public String getSuitAsString() {
        switch (maa) {
            case PATA:
                return "Pata";
            case HERTTA:
                return "Hertta";
            case RUUTU:
                return "Ruutu";
            case RISTI:
                return "Risti";
            default:
                return "Jokeri";
        }
    }

    /**
     * Palauttaa kortin arvon Stringinä
     *
     * @return tavalliselle kortille palautetaan arvo "Ässä", "2", "3", ...,
     * "10", "Jätkä", "Akka", tai "Kunkku". Jokerin arvo on aina numeraalinen.
     */
    public String getValueAsString() {
        switch (arvo) {
                case 1:
                    return "Ässä";
                case 2:
                    return "2";
                case 3:
                    return "3";
                case 4:
                    return "4";
                case 5:
                    return "5";
                case 6:
                    return "6";
                case 7:
                    return "7";
                case 8:
                    return "8";
                case 9:
                    return "9";
                case 10:
                    return "10";
                case 11:
                    return "Jätkä";
                case 12:
                    return "Akka";
                default:
                    return "Kunkku";
            }
    }

    /**
     * Returns a string representation of this card, including both its maa and
 its arvo (except that for a Joker with arvo 1, the return arvo is just
 "Joker"). Sample return values are: "Queen of Hearts", "10 of Diamonds",
     * "Ace of Spades", "Joker", "Joker #2"
     * @return 
     */
    @Override
    public String toString() {
        return getValueAsString() + " of " + getSuitAsString();
    }
    
}
