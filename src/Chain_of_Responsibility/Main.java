/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chain_of_Responsibility;

/**
 *
 * @author petra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int palkka = 3000; 
        System.out.println("Palkkasi on nyt "+ palkka+" euroa");
        System.out.println("Saat palkankorotuksen 1.5%"); 
        
        RaiseHandler rh = new Manager(); 
        double uusiPalkka = (int) rh.HandleRaise(palkka, 0.015); 
        System.out.println("Uusi palkkasi on " +uusiPalkka+" euroa");
        System.out.println("");
       
        System.out.println("Saat palkankorotuksen 4.5%"); 
        uusiPalkka = rh.HandleRaise(palkka, 0.045); 
        System.out.println("Uusi palkkasi on " +uusiPalkka+" euroa");
        System.out.println("");
        
        System.out.println("Saat palkankorotuksen 7.5 %"); 
        uusiPalkka = rh.HandleRaise(palkka, 0.075); 
        System.out.println("Uusi palkkasi on " +uusiPalkka+" euroa");
        System.out.println("");
    }
    
}
