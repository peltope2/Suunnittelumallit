/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chain_of_Responsibility;

/**
 *
 * @author petra
 */
public class Manager implements RaiseHandler {
    
    private RaiseHandler nextHandler; 

    public Manager() {
        nextHandler = new Boss(); 
    }
    
    

    @Override
    public double HandleRaise(double palkka, double korotus) {
         if (korotus <= 0.02){
            System.out.println("Palkkasi korotuksen hyväksyi lähiesimies");
            return palkka*(1+korotus); 
        }
        else {
            return nextHandler.HandleRaise(palkka, korotus); 
        } 
    }
    
}
