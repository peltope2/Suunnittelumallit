package Chain_of_Responsibility;

/**
 *
 * @author petra
 */
public interface RaiseHandler {
    
    public double HandleRaise(double palkka, double korotus);
    
}
