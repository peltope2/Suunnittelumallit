/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chain_of_Responsibility;

/**
 *
 * @author petra
 */
public class CEO implements RaiseHandler {

    @Override
    public double HandleRaise(double palkka, double korotus) {
        System.out.println("Palkkasi korotuksen hyväksyi toimitusjohtaja");
        return palkka*(1+korotus); 
    }
    
    
}
