/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Memento;

import java.util.Random;

/**
 *
 * @author petra
 */
public class Randomizer {
    
    private Random random; 

    public Randomizer() {
        random = new Random(); 
    }
    
// Arpoo pelaajalle arvattavan arvo 1-10
    public void liityPeliin(Pelaaja pelaaja){ 
        pelaaja.setMemento(new Memento(random.nextInt(10)+1)); 
    }
    
    // Vertailee pelaajalle arvotun arvon ja pelaajan arvaamaa arvoa keskenään
    public boolean kasitteleArvaus(int arvaus, Memento memento){ 
        return memento.vertaaArvausta(arvaus); 
    }
}
