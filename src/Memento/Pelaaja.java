
package Memento;

/**
 * 
 * @author petra
 */
public class Pelaaja extends Thread {
    
    private Memento memento; 
    private final Randomizer randomizer; 
    private final String nimi; 

    public Pelaaja(Randomizer randomizer, String name) {
        this.randomizer = randomizer;
        this.nimi = name;
    }

    @Override 
    public void run() { 
        randomizer.liityPeliin(this);
        int arvaus = 1; 
        while (true){ // Tulostaa pelaajat siinä järjestyksessä, kuka arvaa luvun ensimmäisenä. Väärin menneitä arvauksia ei tulosteta. 
            if (randomizer.kasitteleArvaus(arvaus, memento)){
                System.out.println(nimi+ " arvasi oikein!");
                break;
            }
        arvaus++; 
        }
        
    }
    
    public void setMemento(Memento memento) {
       this. memento= memento; 
    }
    
}
