
package Memento;

/**
 *
 * @author petra
 */
public class Memento {
    
    private final int numero; 

    public Memento(int number) {
        this.numero = number;
    }
    
    protected boolean vertaaArvausta (int arvaus){
        return arvaus == numero; 
    }
    
}
