
package Memento;

/**
 *Arvuuttajalla on useita asiakkaita (säikeitä), joita kutakin varten
 * se arpoo luvun, jota pelaaja yrittää arvata. Peli käynnistyy siten, että
 * pelaaja ilmoittautuu peliin mukaan Arvuuttajan liityPeliin()-metodilla
 * (pelaaja välittää itsensä parametrina), Metodissa arvuuttaja arpoo luvun ja
 * tallentaa sen.
 * Arvuuttaja palauttaa liityPeliin()-metodissa pelaajalle Mementon, jossa on 
 * arvottu luku, joten Arvuuttajan ei sitä tarvitse muistaa. 
 * Huomaa, että pelaaja ei saa päästä lukuun käsiksi!
 * Arvauksensa yhteydessä asiakas välittää Arvuuttajalle Mementon, jossa
 * olevaa lukua Arvuuttaja vertaa uuteen arvaukseen ja kertoo kuinka kävi.
 * 
 * 
 * 
 * 
 * @author petra
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Luodaan uusia pelaajia ja arvotaan heille arvattava luku 1-10. 
        
        Randomizer rand = new Randomizer();
        
        Pelaaja p1 = new Pelaaja(rand, "Petra"); 
        Pelaaja p2 = new Pelaaja(rand, "Jani"); 
        Pelaaja p3 = new Pelaaja(rand, "Mikko"); 
        Pelaaja p4 = new Pelaaja(rand, "Tuula"); 
        Pelaaja p5 = new Pelaaja(rand, "Leila");
        Pelaaja p6 = new Pelaaja(rand, "Heikki"); 
        
        p1.start();
        p2.start();
        p3.start();
        p4.start();
        p5.start(); 
        p6.start();
        
        
    }
    
}
