
package Strategy;

/**
 *
 * @author petra
 */

/**
* Pikalajittelu perustuu siihen, että joukosta alkioita valitaan yksi, jonka 
* suhteen joukko jaetaan sitä pienempiin ja sitä suurempiin. Tämän jälkeen
* alussa valittu alkio on järjestyksessä ja sen kummankin puolen on kaksi 
* järjestämätöntä joukkoa. Näistä voidaan samoin valita yksi alkio, jonka
* mukaan ne jaetaan jälleen kahteen pienempään joukkoon; pienempiin ja suurempiin. 
* Tätä joukkojen jakamista jatketaan kunnes alijoukkojen koko on
 *yksi. Tässä vaiheessa alkuperäinen joukko alkioita on päätynyt järjestykseen.
 */

public class QuickSort implements SortStrategy{ 
    void sort(int a[], int lo0, int hi0) throws Exception {
        int lo = lo0;
        int hi = hi0;
        if (lo >= hi) {
            return;
        } else if (lo == hi - 1) {
            
            if (a[lo] > a[hi]) {
                int T = a[lo];
                a[lo] = a[hi];
                a[hi] = T;
            }
            return;
        }
        
        int pivot = a[(lo + hi) / 2];
        a[(lo + hi) / 2] = a[hi];
        a[hi] = pivot;
        while (lo < hi) {
        
            while (a[lo] <= pivot && lo < hi) {
                lo++;
            }
            while (pivot <= a[hi] && lo < hi) {
                hi--;
            }
            if (lo < hi) {
                int T = a[lo];
                a[lo] = a[hi];
                a[hi] = T;
            }
        }
       
        a[hi0] = a[hi];
        a[hi] = pivot;
       
        sort(a, lo0, lo - 1);
        sort(a, hi + 1, hi0);
    }

    @Override
    public void sort(int a[]) throws Exception {
        sort(a, 0, a.length - 1);
    }
}


     


 