
package Strategy;

/**
 *
 * @author petra
 */
public class Strategy {
    
    public static void main(String[] args) {
        final int ARRAY_SIZE = 10000;
        Context context;
        
        int[] a = initArray(ARRAY_SIZE);
        
        System.out.println("Alkioiden lukumäärä " + a.length);
        context = new Context(new QuickSort());
        long startTime = System.currentTimeMillis();
        context.executeStrategy(a);
        long duration = System.currentTimeMillis() - startTime;
        System.out.println("Käytetty aika (Pikalajittelu): " + (duration) + " millisekuntia");
        
        
        a = initArray(ARRAY_SIZE);
        context = new Context(new BubbleSort());
        startTime = System.currentTimeMillis();
        context.executeStrategy(a);
        duration = System.currentTimeMillis() - startTime;
        System.out.println("Käytetty aika (Kuplalajittelu): " + (duration) + " millisekuntia");
        
        a = initArray(ARRAY_SIZE);
        context = new Context(new SelectionSort());
        startTime = System.currentTimeMillis();
        context.executeStrategy(a);
        duration = System.currentTimeMillis() - startTime;
        System.out.println("Käytetty aika (Valintalajittelu): " + (duration) + " millisekuntia");
    }
    
    public static int[] initArray(int size) {
        int[] a = new int[size];
        int min = Integer.MIN_VALUE;
        int max = Integer.MAX_VALUE;
        for (int i = 0; i < size; i++) {
            a[i] = min + (int) (Math.random() * ((max - min) + 1));
        }
        return a;
    }
}
    

