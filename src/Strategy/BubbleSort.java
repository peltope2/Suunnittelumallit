
package Strategy;

/**
 *
 * @author petra
 */

/** 
 * Kuplalajittelussa vertaillaan kahta peräkkäistä alkiota keskenään.
 * 1. Jos ne ovat väärässä järjestyksessä, vaihdetaan ne keskenään.
 * 2. Mikäli vaihtoja tehtiin, toistetaan ensimmäinen vaihe.
*/

public class BubbleSort implements SortStrategy { 
        private SortStrategy strategy;

    @Override
    public void sort(int[] a) throws Exception {
        for (int i= a.length; --i >=0; ){
            boolean flipped = false; 
            for (int j=0; j< i; j++){
                int T = a[j]; 
                a[j]= a[j+1]; 
                a[j+1]=T;
                flipped= true; 
            }
        }
    }
 
        
    
}
