
package Strategy;

/**
 *
 * @author petra
 */

/**
 * Valintalajittelu algoritmi toimii siten, että 
 * Etsitään taulukon pienin alkio ja lisätään se järjestettyyn taulukon ensimmäiseksi.
 * Tämän jälkeen etsitään seuraavaksi pienin alkio ja lisätään se järjestettyyn taulukkoon seuraavaksi.
 * Kun toiseksi viimeinen alkio on saatu käsiteltyä, koko taulukko on järjestyksessä.
 */

public class SelectionSort implements SortStrategy { 

    @Override
    public void sort(int[] a) throws Exception {
        for (int i = 0; i < a.length; i++) {
            int min = i;
            int j;
            /* * Etsitään listan pienin alkio */
            for (j = i + 1; j < a.length; j++) {
                if (a[j] < a[min]) {
                    min = j;
                }
            }
            /* * Alkio siirretään järjestettyyn taulukkoon viimeiseksi. */
            int T = a[min];
            a[min] = a[i];
            a[i] = T;
        }
    }
}
