/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorate;

import java.util.ArrayList; 

/**
 *
 * @author petra
 */
public class Pizzeria {
    public static void main(String[] args) {
        ArrayList<PizzaIF> hinnasto = new ArrayList<>();
        hinnasto.add(new Ananas(new Sinihomejuusto (new PizzaPohja(new Pizza()))));
        hinnasto.add(new Kana(new PizzaPohja(new Pizza())));
        hinnasto.add(new Sinihomejuusto(new Ananas(new Kana(new PizzaPohja(new Pizza())))));
        for (PizzaIF pizza :
                hinnasto) {
            System.out.println(pizza.getDescription()+" "+pizza.getPrice()+"€");
        }
    }
}
