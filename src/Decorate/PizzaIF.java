/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorate;

/**
 *
 * @author petra
 */
public interface PizzaIF {
    String getDescription();
    double getPrice();

}
