/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorate;

/**
 *
 * @author petra
 */
public class Sinihomejuusto extends PizzaDecorator {
    public Sinihomejuusto(PizzaIF filling) {
        super(filling);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + " Sinihomejuusto";
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 2.00;
    }
}
