/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorate;

/**
 *
 * @author petra
 */
abstract public class PizzaDecorator implements PizzaIF {
    protected PizzaIF tayte;

    public PizzaDecorator(PizzaIF filling) {
        this.tayte = filling;
    }

    @Override
    public String getDescription() {
        return tayte.getDescription();
    }

    @Override
    public double getPrice() {
        return tayte.getPrice();
    }

    @Override
    public String toString() {
        return "PizzaDecorator{" +
                "description=" + tayte.getDescription() +
                "price=" + tayte.getPrice() +
                '}';
    }
}