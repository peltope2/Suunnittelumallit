/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

import State.*;

/**
 *
 * @author petra
 */
public class Charizard implements State {
    
    @Override
    public void hyokkaa() {
        System.out.println("Osuin vastustajaan ja aiheutin sille 84 pisteen menetyksen "); 
    }

    @Override
    public void puolusta() {
        System.out.println("Puolustan itseäni sylkemällä tulta vastustajaa päin."); 
    }

    @Override
    public void pakene() {
        System.out.println("Apua, vastustaja on vahvempi. Tekisi mieli paeta! "); 
    }

    @Override
    public void tulostaTila() {
        System.out.println("Olen Charizard!"); 
    }
    
    @Override
    public void accept(Visitor visitor) {
    visitor.visit(this);
    }
 
}
