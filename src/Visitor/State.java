/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

import State.*;

/**
 *
 * @author petra
 */
public interface State {
    
    public void hyokkaa(); 
    public void puolusta();
    public void pakene(); 
    public void tulostaTila();
    public void accept(Visitor visitor); 
}
    

