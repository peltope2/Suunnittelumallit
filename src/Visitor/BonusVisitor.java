/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visitor;

/**
 *
 * @author petra
 */
public class BonusVisitor implements Visitor{

    @Override
    public void visit(Charmander charmander) {
        System.out.println ("Ansaitsit 1 bonuspisteen");
    }

    @Override
    public void visit(Charmeleon charmeleon) {
        System.out.println ("Ansaitsit 5 bonuspistettä");
    }

    @Override
    public void visit(Charizard charizard) {
        System.out.println ("Ansaitsit 10 bonuspistettä");
    }
 
    
}
