/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author petra
 */
public abstract class Adidastehdas implements Vaatetehdas{

    public Housut makeHousut() {
        return new Adidashousut();
    }

    public Paita makepaita() {
        return new Adidaspaita();
    }

    public Lippis makeLippis() {
        return new Adidaslippis();
    }

    public Kengat makeKengat() {
        return new Adidaskengat();
    }

}

