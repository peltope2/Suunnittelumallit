/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author petra
 */
public class Jasper {
    private Paita paita;
    private Housut housut; 
    private Kengat kengat; 
    private Lippis lippis; 

    public Jasper (Vaatetehdas tehdas) {
        paita = tehdas.makePaita(); 
        housut = tehdas.makeHousut(); 
        kengat = tehdas.makeKengat(); 
        lippis = tehdas.makeLippis(); 
    }
    
    public String toString() {
        String tulostus = "Päälläni on: \n";
        
        tulostus += housut + "\n";
        tulostus += kengat + "\n";
        tulostus += lippis + "\n";
        tulostus += paita + "\n";
        
        return tulostus;
    }
}
