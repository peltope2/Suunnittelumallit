/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author petra
 */
public class Bosstehdas implements Vaatetehdas {

    public Housut makeHousut() {
        return new Bosshousut();
    }

    public Paita makePaita() {
        return new Bosspaita();
    }

    public Lippis makeLippis() {
        return new Bosslippis();
    }

    public Kengat makeKengat() {
        return new Bosskengat();
    }

}

