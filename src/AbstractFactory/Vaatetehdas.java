/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractFactory;

/**
 *
 * @author petra
 */
public interface Vaatetehdas {
    public abstract Housut makeHousut();
    public abstract Paita makePaita();
    public abstract Lippis makeLippis();
    public abstract Kengat makeKengat();
    
}
