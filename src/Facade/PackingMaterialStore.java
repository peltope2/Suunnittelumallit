/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author petra
 */
class PackingMaterialStore implements Store {

    @Override
    public Goods getGoods() {
        PackingMaterialGoods packingGoods = new PackingMaterialGoods(); 
        return packingGoods; 
    }
    
}
