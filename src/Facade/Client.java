/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author petra
 */
public class Client {
    public static void main(String[] args) {
        StoreKeeper storeKeeper = new StoreKeeper();
       
        System.out.println("Getting finished goods");
        System.out.println("Got: " + storeKeeper.getGoods("Finished").getName());
        
        System.out.println("Getting packing goods");
        System.out.println("Got: " + storeKeeper.getGoods("Packing").getName());
        
        System.out.println("Getting raw material goods");
        System.out.println("Got: " + storeKeeper.getGoods("").getName());
    }
}