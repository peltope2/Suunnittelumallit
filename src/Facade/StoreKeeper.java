/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facade;

/**
 *
 * @author petra
 */
public class StoreKeeper {
	/**
        * The common method
        *
        * @return Goods
        */
        public Goods getGoods(String goodsType) {
        if (goodsType.equals("Packaging")) {
        PackingMaterialStore store = new PackingMaterialStore();
        PackingMaterialGoods packingMaterialGoods = (PackingMaterialGoods)store.getGoods();
        return packingMaterialGoods;
        }
        else if (goodsType.equals("Finished")) {
        FinishedGoodsStore store = new FinishedGoodsStore();
        FinishedGoods finishedGoods = (FinishedGoods)store.getGoods();
        return finishedGoods;
        }
        else {
        RawMaterialStore store = new RawMaterialStore();
        RawMaterialGoods rawMaterialGoods = (RawMaterialGoods)store.getGoods();
        return rawMaterialGoods;
        }

    }

}