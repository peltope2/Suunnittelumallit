/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author petra
 */
public class TestScreen {


    public static void main(String[] args) {
        
        Screen whitescreen = new Screen(); 
        Command switchUp = new FlipUpCommand(whitescreen); 
        Command switchDown = new FlipDownCommand(whitescreen); 
        WallButton button1 = new WallButton(switchUp); 
        WallButton button2 = new WallButton (switchDown); 
        
        button1.push();
        button2.push(); 
    }
    
}
